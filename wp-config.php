<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'oinkdigital' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p^6m$U@s1mbL2WWl:}(K)(U>K+jzh_O=23x.5k^wft[>nOtDsrsYxOhJ)>+|}]TB' );
define( 'SECURE_AUTH_KEY',  'H@j9zDG>uUuFR0~TK$+V++rmj3!-$ryOM&}fmkk3c<lzGc!,Dc;ofrI;DX[tTf69' );
define( 'LOGGED_IN_KEY',    'Ua`~*f%u?fQ+45eUF<Rx^#CKNvY8}q0@{5]hE+-1ysNrNJtpf[[-/3F?y7:gS76W' );
define( 'NONCE_KEY',        'tM[?4cnL}2UaHvo%tlvAi0gU1*Xl0+X!Eao6GhN)/&.E^=047BvSnUTuRL2,9L!{' );
define( 'AUTH_SALT',        '_>RfENvU~nw[G_m85xeGk>##y?u$1(4mTmrO160XwC?6*5,b@r_W]`-5k^)~&Y6N' );
define( 'SECURE_AUTH_SALT', 'CRjlSrAdz%YTXEIG1*yR{}_nEEP~eBPe%r<sQ;1lc#d9*Nl9>t5tj<H0P5U9qx_?' );
define( 'LOGGED_IN_SALT',   'Xkx2jrQz+10aXUKQZ$~PK=K5>R*]s}*-Y#NBxRg$R8/ LomJRY*0L0LgK<TiY+I;' );
define( 'NONCE_SALT',       'O6}c=Hoj*`A$?ziLH Ad-iTFhI_O6}x0<}V5aG@P{@e:xBkws*udW$4&^u,H,+-`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'oink_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
