<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Stylesheet Override
Plugin URI:  http://wordpress.com
Description: Add close and session function to the top banner
Version:     1.0
Author:      Oink Digital
Author URI:  http://oinkdigital.com.au
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
add_action( 'wp_enqueue_scripts', 'cssOverride' );
function cssOverride(){
	wp_enqueue_style('oink-css-override',plugins_url('css/plugin.css',__FILE__));
}