<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Owler
Plugin URI:  http://wordpress.com
Description: Owl carousel plugin with everything done specifically for streetadvocate
Version:     1.0
Author:      Chris Asher
Author URI:  http://chrisasher.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

function owler_shortcode() {

	$args = array(
		'posts_per_page'   => 9,
//		'orderby'          => 'date',
//		'order'            => 'DESC',
		'post_type'        => 'owler',
		'post_status'      => 'publish',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
		'meta_key' => '_c_order'
	);
	$posts_array = get_posts( $args );
	$out = '';
	// owl controls
//	$out .= '<div id="owl-prev" class="owl-custom-nav"><span class="arrow-left"></span></div>';
//	$out .= '<div id="owl-next" class="owl-custom-nav"><span class="arrow-right"></span></div>';

	$out .= '<div class="owlCarousel">';

	foreach($posts_array as $post){
		setup_postdata($post);
		$meta=array();
		$meta = get_post_meta($post->ID);

		$label = array();
		$labels = (get_field('item-label',$post->ID))? get_field('item-label',$post->ID) : array();

		if(count($labels)){
			foreach($labels as $l){
				$label[$l] = $l;
			}
		}

		if(has_post_thumbnail($post->ID)){
			$img = $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];
			$background = 'style="background:url(\''.$img.'\') no-repeat center center; background-size:cover;"';
		}

		$out .= '<div class="item" '.$background.'>';
		$out .= '<div class="row">';
		$out .= '<div class="slide-content gr-6 push-3">';
		if(isset($label['new'])){
			$out .= '<img src="'.get_stylesheet_directory_uri().'/imgs/ycc-new-icon.png" class="new-label" />';
		}
		$out .= (get_the_title($post->ID))? '<h3>'.get_the_title($post->ID).'</h3>' : '';
		$out .= (get_the_content())? '<p>'.get_the_content().'</p>' : '';
		if($meta['_c_link_url'][0] && $meta['_c_link_text'][0]){
			$out .= '<a class="btn white slider-link" href="'.$meta['_c_link_url'][0].'">'.$meta['_c_link_text'][0].'</a>';
		}
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';

	}
	wp_reset_postdata();
	$out .= '</div>';


	return $out;
}
add_shortcode( 'owler', 'owler_shortcode' );


add_action( 'wp_enqueue_scripts', 'loadOwlerScripts' );
function loadOwlerScripts(){
	wp_enqueue_style('stylesheet',plugins_url('css/style.css',__FILE__));
	wp_enqueue_style('owlstylesheet',plugins_url('css/owl.carousel.css',__FILE__));
	wp_enqueue_script('owlcarousel',plugins_url('js/owl.carousel.min.js',__FILE__),array('jquery'));
	wp_enqueue_script('owlscript',plugins_url('js/owler.js',__FILE__),array('jquery'));

}

add_action( 'init', 'carousel_post_type' );
function carousel_post_type(){
	register_post_type('owler',array(
		'labels' => array(
			'name' => __( 'Carousel' ),
			'singular_name' => __( 'Slide' )
		),
		'supports' => array( 'thumbnail', 'title', 'editor' ),
		'public' => true,
		'has_archive' => true,
		'register_meta_box_cb' => 'add_order_metabox',
	));
}

function add_order_metabox(){
	add_meta_box('_c_order', 'Order', 'order_input_field', 'owler', 'side', 'default');
	add_meta_box('_c_link_url', 'Link URL', 'url_input_field', 'owler');
	add_meta_box('_c_link_text', 'Link Text', 'link_text_input_field', 'owler');
}

function order_input_field($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'owler_save_meta_box_data', 'owler_meta_box_nonce' );

	$order = get_post_meta($post->ID, '_c_order', true);
	echo '<input id="_c_order" class="fullfat" type="number" name="_c_order" value="'.$order.'">';
}

function url_input_field($post){
	$url = get_post_meta($post->ID, '_c_link_url', true);
	echo '<input id="_c_link_url" class="fullfat" type="text" name="_c_link_url" value="'.$url.'">';
}

function link_text_input_field($post){
	$link_text = get_post_meta($post->ID, '_c_link_text', true);
	echo '<input id="_c_link_text" class="fullfat" type="text" name="_c_link_text" value="'.$link_text.'">';
}

function owler_save_meta_box_data( $post_id ) {
	// Check if our nonce is set.
	if ( ! isset( $_POST['owler_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['owler_meta_box_nonce'], 'owler_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'owler' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */

	// Make sure that it is set.
	if ( !isset( $_POST['_c_order']) && !isset( $_POST['_c_link_url']) && !isset( $_POST['_c_link_text']) ) {
		return;
	}

	// Sanitize user input.
	$order = sanitize_text_field( $_POST['_c_order'] );
	$url = sanitize_text_field( $_POST['_c_link_url'] );
	$link_text = sanitize_text_field( $_POST['_c_link_text'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_c_order', $order );
	update_post_meta( $post_id, '_c_link_url', $url );
	update_post_meta( $post_id, '_c_link_text', $link_text );
}
add_action( 'save_post', 'owler_save_meta_box_data',1,3);
