/**
 * Created by chris on 13/11/2015.
 */
jQuery(document).ready(function($){
	var owl = $('.owlCarousel');
	owl.owlCarousel({
		loop:true,
		//center:true,
		nav:false,
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		autoplaySpeed:1000,
		items:1,
		dots:true
	})

	$("#owl-prev").click(function(){
		owl.trigger('prev.owl.carousel');
	})
	$("#owl-next").click(function(){
		owl.trigger('next.owl.carousel');
	})
});