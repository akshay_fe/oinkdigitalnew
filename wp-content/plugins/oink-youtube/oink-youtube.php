<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Oink YouTube
Plugin URI:  http://wordpress.com
Description: Youtube shortcode [oinkyoutube video="video_id"]
Version:     1.0
Author:      Chris Asher
Author URI:  http://oinkdigital.com.au
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
	include_once("youtube-api/Constants.php");
	include_once("youtube-api/Youtube.php");
	include_once("youtube-api/compat.php");


function oink_youtube_shortcode($atts) {
	
		// Attributes
	extract( shortcode_atts(
			array(
				'vid_id' => '',
				'class' => '',
			), $atts )
	);

	
	$youtube = new Madcoda\Youtube\Youtube(array('key' => 'AIzaSyBZw8Zu9wPxA1i9xAyYtb2FVUdoz0OD4xI'));
	$video = $youtube->getVideoInfo($vid_id);
//	print_v($video);
	
	$out = '';
	$out .= '<a href="https://www.youtube.com/watch?v='.$vid_id.'" class="youtube-thumbnail" data-fancybox data-caption="'.$video->snippet->title.'"><img src="'.$video->snippet->thumbnails->maxres->url.'" /><span class="overlay"><span class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i><br>PLAY</span></span></a>';
	
	return $out;
}

add_shortcode( 'oink-youtube', 'oink_youtube_shortcode' );


add_action( 'wp_enqueue_scripts', 'load_oink_youtube' );
function load_oink_youtube(){
//	wp_enqueue_script('oink-youtube',plugins_url('js/oink-youtube.js',__FILE__),array('jquery'));
}
