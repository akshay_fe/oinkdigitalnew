<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Griddle rows and columns
Plugin URI:  http://wordpress.com
Description: generate rows and columns for the griddle css grid
Version:     1.0
Author:      Chris Asher
Author URI:  http://chrisasher.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

function grid_section( $atts ) {
	// Attributes
	extract( shortcode_atts(
			array(
				'class' => '',
				'background' => '',
			), $atts )
	);

	if($background){
		$class .= ' w-bg-img';
	}

	// Code
	$out = '</div>';
	$out .= '</section>';
	$out .= '<section class="content '.$class.'" '.(($background)?'style="background-image:url(\''.$background.'\')"':'').'>';
	$out .= '<div class="container">';
	return $out;
}
add_shortcode( 'section', 'grid_section' );

function grid_row( $atts , $content = null ) {
	// Attributes
	extract( shortcode_atts(
			array(
				'class' => '',
			), $atts )
	);

	// Code
	$out = '<div class="row '.$class.'">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	return $out;
}
add_shortcode( 'row', 'grid_row' );

function grid_col( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
			array(
				'width' => 12,
				'class' => '',
			), $atts )
	);
	$width = (int) $width;

	// Code
	$out = '<div class="gr-'.$width.' '.$class.'">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	return $out;
}
add_shortcode( 'col', 'grid_col' );


remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );

