<?php
get_header();
?>
<section id="homepage-hero">
	<?php echo do_shortcode('[owler]'); ?>
	<div class="scroll-btn">
		Scroll<br/>
		<i class="fa fa-chevron-down" aria-hidden="true"></i>
	</div>
</section>
<section id="introText" class="content container">
	<div class="row">
		<div class="gr-12">
			<h1><?php the_title() ?></h1>
			<?php the_field("intro") ?>
		</div>
	</div>
</section>
<hr>
<section id="services" class="content no-bottom-padding">
	<div class="row">
	
		<div class="gr-12">
			<h2>Our Specialties</h2>
		</div>
	
		<a class="gr-4 service-panel service-1" href="<?php echo get_theme_mod("service1_page_url") ?>"><span class="service-text">Web Design</span><span class="learn-more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
		<a class="gr-4 service-panel service-2" href="<?php echo get_theme_mod("service2_page_url") ?>"><span class="service-text">Video Strategy</span><span class="learn-more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
		<a class="gr-4 service-panel service-3" href="<?php echo get_theme_mod("service3_page_url") ?>"><span class="service-text">Podcasting</span><span class="learn-more">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
	</div>
</section>

<section class="content">
	<div class="container">
		<?php the_content() ?>
	</div>
</section>

<?php
get_footer();