<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '453658434836544');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=453658434836544&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<div id="site" class="site fade-effect">
<body id="body" <?php body_class(); ?>>
    <div class="mobile-logo mobile">
    <a href="<?php echo get_home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri() ?>/imgs/logo.png" /></a>
    </div>
    <section id="header" class="display desktop">
    <div class="container">
        <div class="row">
            <div class="gr-2">
                <a href="<?php echo get_home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri() ?>/imgs/logo.png" /></a>
            </div>
            <div class="gr-10 no-gutter-top no-gutter-bottom">
                <nav>
                    <?php
                    wp_nav_menu(array('theme_location' => 'primary'));
                    ?>
                </nav>
            </div>
        </div>
    </div>
    </section>

