
<section id="cta" class="bgc-2 content">
	<div class="container">
		<div class="row">
			<div class="gr-12 text-center">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/imgs/yt_logo.png" class="youtube-logo"/>
				<?php 
				if(is_active_sidebar("cta")){
					dynamic_sidebar("cta");
				}
				?>
			</div>
		</div>
	</div>
</section>

<section id="footer" class="bgs-3">
	<div class="container">
		<div class="row">
			<div class="gr-4 text-center">
				<?php 
				if(is_active_sidebar("footer-left")){
					dynamic_sidebar("footer-left");
				}
				?>
			</div>
			<div class="gr-4 text-center">
				<?php 
				if(is_active_sidebar("footer-middle")){
					dynamic_sidebar("footer-middle");
				}
				?>
			</div>
			<div class="gr-4">
				<?php 
				if(is_active_sidebar("footer-right")){
					dynamic_sidebar("footer-right");
				}
				?>
			</div>
			<div class="gr-12 text-center smalltext">
				&copy; OINK Digital <?php echo date("Y"); ?>
			</div>
		</div>
	</div>
</section>
</div>
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53450590-5', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
