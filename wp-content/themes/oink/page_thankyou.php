<?php
//* Template Name: Thankyou
get_header();

?>
<script>
fbq('track', 'Lead');
</script>
<?php
// Start the loop.
if(have_posts()){
	while ( have_posts() ){
		the_post();

		$image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg';
		$title = get_the_title();
		$id = get_the_ID();
		$bodyclass = get_body_class();

		?>
		<section class="page-head">
			<div class="bg-img w-bg-img fixed" style="background-image: url(<?php echo $image ?>)"></div>
			<div class="page-title">
				<div class="gr-12">
					<h1 class="no-margin"><?php the_title() ?></h1>
				</div>
			</div>
		</section>
		<section id="introText" class="content container">
			<div class="row">
				<div class="gr-12">
					<?php the_field("intro") ?>
				</div>
			</div>
		</section>
		<hr>
		<section class="content">
			<div class="container">
				<?php the_content() ?>
			</div>
		</section>
		<?php
		// End of the loop.
	}
}
?>

<?php
get_footer();