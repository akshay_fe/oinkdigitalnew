<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( ! function_exists( 'theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own theme_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function theme_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		add_theme_support( 'woocommerce' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'theme' ),
			'secondary'  => __( 'Secondary', 'theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
//		add_theme_support( 'html5', array(
//			'search-form',
//			'comment-form',
//			'comment-list',
//			'gallery',
//			'caption',
//		) );

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
//		add_theme_support( 'post-formats', array(
//			'aside',
//			'image',
//			'video',
//			'quote',
//			'link',
//			'gallery',
//			'status',
//			'audio',
//			'chat',
//		) );

	}
endif; // theme_setup
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'CTA', 'theme' ),
		'id'            => 'cta',
		'description'   => __( 'Appears on the cta above the footer', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer left', 'theme' ),
		'id'            => 'footer-left',
		'description'   => __( 'Appears on the footer left', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer middle', 'theme' ),
		'id'            => 'footer-middle',
		'description'   => __( 'Appears on footer middle', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer right', 'theme' ),
		'id'            => 'footer-right',
		'description'   => __( 'Appears on footer right	', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	) );


}
add_action( 'widgets_init', 'theme_widgets_init' );


/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function theme_scripts() {

    // Theme stylesheet.
	wp_enqueue_style( 'fontawesome-style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css' );
	wp_enqueue_style( 'roboto-font-style', 'https://fonts.googleapis.com/css?family=Roboto:300' );
	wp_enqueue_style( 'opensans-font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet' );
	wp_enqueue_style( 'theme-style', get_stylesheet_uri() );

//	wp_enqueue_script( 'masonry-js',
//		get_stylesheet_directory_uri().'/plugs/masonry.js',
//		array( 'jquery' )
//	);
	wp_enqueue_script( 'fancybox-js',
		get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.js',
		array( 'jquery' )
	);

	wp_enqueue_script( 'theme-nav', get_template_directory_uri() . '/plugs/slicknav/jquery.slicknav.min.js', array('jquery'));
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/main.js', array('jquery'));

    wp_enqueue_style('fancybox-css',get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.css');
//	wp_enqueue_style('woo-small-css',get_home_url().'/wp2016/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css', array(),'','all and (max-width:800px)');


}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function theme_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'theme_body_classes' );




/*
 * Custom theme options
 */
$args = array(
	'width'         => 980,
	'height'        => 60,
	'default-image' => '',
);
add_theme_support( 'custom-header', $args );

function theme_customize_register( $wp_customize ) {
	/*
	 * social links start
	 */
	$wp_customize->add_section( 'theme_social_links' , array(
		'title'      => __( 'Social Links', 'theme' ),
		'priority'   => 30,
	) );
	// facbook
	$wp_customize->add_setting( 'facebook_link' , array(
		'default' => 'http://',
	) );
	//twitter
	$wp_customize->add_setting( 'twitter_link' , array(
		'default' => 'http://',
	) );
	// instagram
	$wp_customize->add_setting( 'instagram_link' , array(
		'default' => 'http://',
	) );
	// Youtube
	$wp_customize->add_setting( 'youtube_link' , array(
		'default' => 'http://',
	) );
	// Linkedin
	$wp_customize->add_setting( 'linkedin_link' , array(
		'default' => 'http://',
	) );

	$wp_customize->add_control(
		'facebook_link', array(
			'label' => __('Facebook Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'facebook_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'twitter_link', array(
			'label' => __('Twitter Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'twitter_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'instagram_link', array(
			'label' => __('Instagram Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'instagram_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'youtube_link', array(
			'label' => __('YouTube Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'youtube_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'linkedin_link', array(
			'label' => __('LinkedIn Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'linkedin_link',
			'type' => 'url',
		)
	);
	/*
	 * social links end
	 */

	/*
	 * Address Info Start
	 */
	$wp_customize->add_section( 'theme_contact_info' , array(
		'title'      => __( 'Contact Info', 'theme' ),
		'priority'   => 30,
	) );


	// Office street address
	$wp_customize->add_setting( 'place' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'place', array(
			'label' => __('Place Name', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'place',
			'type' => 'text',
		)
	);

	// Office street address
	$wp_customize->add_setting( 'office_street_address' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_street_address', array(
			'label' => __('Office Street Address', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_street_address',
			'type' => 'text',
		)
	);

	// Office city
	$wp_customize->add_setting( 'office_city' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_city', array(
			'label' => __('Office City', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_city',
			'type' => 'text',
		)
	);
	// Office state
	$wp_customize->add_setting( 'office_state' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_state', array(
			'label' => __('Office State', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_state',
			'type' => 'text',
		)
	);

	// Office postcode
	$wp_customize->add_setting( 'office_postcode' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_postcode', array(
			'label' => __('Office Postcode', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_postcode',
			'type' => 'text',
		)
	);

	// Office Country
	$wp_customize->add_setting( 'office_country' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_country', array(
			'label' => __('Office Country', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_country',
			'type' => 'text',
		)
	);

	// Contact Phone
	$wp_customize->add_setting( 'contact_phone' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_phone', array(
			'label' => __('Contact Phone', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_phone',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_email' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_email', array(
			'label' => __('Contact Email', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_email',
			'type' => 'text',
		)
	);
	
	
		/*
	 * Services Info Start
	 */
	$wp_customize->add_section( 'theme_services' , array(
		'title'      => __( 'Services', 'theme' ),
		'priority'   => 30,
	) );


	// Service 1
	$wp_customize->add_setting( 'service1_page_url' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'service1_page_url', array(
			'label' => __('Service 1 Page Link', 'theme'),
			'section' => 'theme_services',
			'settings' => 'service1_page_url',
			'type' => 'url',
		)
	);

	// Service 2
	$wp_customize->add_setting( 'service2_page_url' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'service2_page_url', array(
			'label' => __('Service 2 Page Link', 'theme'),
			'section' => 'theme_services',
			'settings' => 'service2_page_url',
			'type' => 'url',
		)
	);
	
	// Service 3
	$wp_customize->add_setting( 'service3_page_url' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'service3_page_url', array(
			'label' => __('Service 3 Page Link', 'theme'),
			'section' => 'theme_services',
			'settings' => 'service3_page_url',
			'type' => 'url',
		)
	);
	

}
add_action( 'customize_register', 'theme_customize_register' );


/* customposts specific to the theme */

//function homepage_features() {
//
//	$labels = array(
//		'name'                  => 'Homepage Features',
//		'singular_name'         => 'Homepage Feature',
//		'menu_name'             => 'Homepage Features',
//		'name_admin_bar'        => 'Homepage Features',
//		'archives'              => 'Item Archives',
//		'parent_item_colon'     => 'Parent Item:',
//		'all_items'             => 'All Items',
//		'add_new_item'          => 'Add New Item',
//		'add_new'               => 'Add New',
//		'new_item'              => 'New Item',
//		'edit_item'             => 'Edit Item',
//		'update_item'           => 'Update Item',
//		'view_item'             => 'View Item',
//		'search_items'          => 'Search Item',
//		'not_found'             => 'Not found',
//		'not_found_in_trash'    => 'Not found in Trash',
//		'featured_image'        => 'Featured Image',
//		'set_featured_image'    => 'Set featured image',
//		'remove_featured_image' => 'Remove featured image',
//		'use_featured_image'    => 'Use as featured image',
//		'insert_into_item'      => 'Insert into item',
//		'uploaded_to_this_item' => 'Uploaded to this item',
//		'items_list'            => 'Items list',
//		'items_list_navigation' => 'Items list navigation',
//		'filter_items_list'     => 'Filter items list',
//	);
//	$args = array(
//		'label'                 => 'Homepage Feature',
//		'description'           => 'Feature blocks forthe homepage',
//		'labels'                => $labels,
//		'supports'              => array( 'title', 'editor', 'thumbnail' ),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'page',
//	);
//	register_post_type( 'homepage_feature', $args );
//
//}
//add_action( 'init', 'homepage_features', 0 );



//misc functions used in the theme

function get_the_excerpt_max_charlength($id, $charlength) {
	$excerpt = get_the_excerpt($id);
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$out = mb_substr( $subex, 0, $excut );
		} else {
			$out = $subex;
		}
		$out .= '....';
	} else {
		$out = $excerpt;
	}
	return $out;
}

function print_v($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

// Add Shortcode
function btn_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
			array(
				'url' => '',
				'class' => '',
				'target' => '',
			), $atts )
	);

	// Code
	$out = '<a class="button '.$class.'" href="'.$url.'" target="'.$target.'">';
	$out .= $content;
	$out .= '</a>';
	return $out;

}
add_shortcode( 'theme-btn', 'btn_shortcode' );

function social_shortcode() {

	$out = '';
	
	if(get_theme_mod('facebook_link')){
		$out .= '<a class="social-link facebook-link" href="'.get_theme_mod('facebook_link').'" target="_blank">';
		$out .= '<i class="fa fa-facebook" aria-hidden="true"></i>';
		$out .= '</a>';
	}
	if(get_theme_mod('youtube_link')){
		$out .= '<a class="social-link youtube-link" href="'.get_theme_mod('youtube_link').'" target="_blank">';
		$out .= '<i class="fa fa-youtube-play" aria-hidden="true"></i>';
		$out .= '</a>';
	}
	if(get_theme_mod('instagram_link')){
		$out .= '<a class="social-link instagram-link" href="'.get_theme_mod('instagram_link').'" target="_blank">';
		$out .= '<i class="fa fa-instagram" aria-hidden="true"></i>';
		$out .= '</a>';
	}
	if(get_theme_mod('twitter_link')){
		$out .= '<a class="social-link twitter-link" href="'.get_theme_mod('twitter_link').'" target="_blank">';
		$out .= '<i class="fa fa-twitter" aria-hidden="true"></i>';
		$out .= '</a>';
	}
	return $out;

}
add_shortcode( 'social-links', 'social_shortcode' );


function custom_wp_trim_excerpt($text, $wordcount=500) {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		//Retrieve the post content.
		$text = get_the_content('');
	}
	//Delete all shortcode tags from the content.
	$text = strip_shortcodes( $text );

	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);

	$allowed_tags = ''; /*** MODIFY THIS. Add the allowed HTML tags separated by a comma.***/
	$text = strip_tags($text, $allowed_tags);

	$excerpt_word_count = $wordcount; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
	$excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);

	$excerpt_end = ''; /*** MODIFY THIS. change the excerpt endind to something else.***/
	$excerpt_more = apply_filters('excerpt_more', '' . $excerpt_end);

	$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
	if ( count($words) > $excerpt_length ) {
		array_pop($words);
		$text = implode(' ', $words);
		$text = $text . $excerpt_more;
	} else {
		$text = implode(' ', $words);
	}
	return $text;
//	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}


// remove woocommerce styles if ther are there
add_filter( 'woocommerce_enqueue_styles', '__return_false');


//function woocommerce_cat_to_class($cat){
//
//	switch($cat){
//		case 'art-style':
//			$class = 'art';
//			break;
//
//		case 'computers-business':
//			$class = 'gov';
//			break;
//
//		case 'cooking-hospitality':
//			$class = 'child';
//			break;
//
//		case 'dance-music':
//			$class = 'music';
//			break;
//
//		case 'health-wellbeing':
//			$class = 'health';
//			break;
//
//		case 'kids-youth':
//			$class = 'main';
//			break;
//
//		case 'languages':
//			$class = 'lang';
//			break;
//
//		case 'people-with-disability':
//			$class = 'comm';
//			break;
//
//		default:
//			$class = 'main';
//			break;
//	}
//	return $class;
//}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

// shortcodes in widgets
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');


// remove image height and width
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
	$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	return $html;
}

//add_filter( 'gform_ajax_spinner_url', 'my_wpcf7_ajax_loader' );
//add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
//function my_wpcf7_ajax_loader () {
//	return  get_bloginfo('stylesheet_directory') . '/imgs/301.gif';
//}
