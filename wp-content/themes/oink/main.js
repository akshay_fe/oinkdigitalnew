/**
 * Created by chris on 12/04/2016.
 */


jQuery(document).ready(function($){

	$('#site').addClass('fade-effect');
	$('#load-container').addClass('loading');
	$('#load').addClass('loading');

	$('#menu-main-menu').slicknav({
		label: '',
		//prependTo: "#header",
		closeOnClick:true,
		beforeOpen: function(){
			$(".adaptable").slideDown('fast');
		},
		beforeClose:function(){
			$(".adaptable").slideUp('fast');
		}
	});

	fullscreen('.owlCarousel .item');
	posSlideContent();
	$(".scroll-btn").click(function(){
		$('html, body').animate({
			scrollTop: $("#introText").offset().top - 100
		}, 1000);
	});
	
	makesquare(".service-panel");
	
	var lastScrollTop = 0;
	$(window).scroll(function(event){
	 	var st = $(this).scrollTop();
	 	if (st > lastScrollTop && st > 100){
		   // downscroll code
			$("#header").removeClass("display");
	 	} else {
		  // upscroll code
			$("#header").addClass("display");
	 	}
	 	lastScrollTop = st;
	});
	
	
	
});

jQuery(window).load(function(){
	var $ = jQuery;
	$('#site').removeClass('fade-effect');
	$('#load-container').removeClass('loading');
	$('#load').removeClass('loading');
});

jQuery(window).resize(function($){

});

function sizetorow(divheight, target, adjustment){
	var $ = jQuery;
	if($(window).width() > 800){
		var maxHeight = 0;

		$(target).css('min-height',maxHeight);
		maxHeight = $(divheight).height() - adjustment;
		console.log(maxHeight);
		//$(target).each(function(){
		//
		//})
		setTimeout(function(){
			$(target).css('min-height',maxHeight);
		},600);
	}
}

function makesquare(target){
	var $ = jQuery;
	var width = $(target).width();
	$(target).height(width);
}

function fullscreen(target){
	var $ = jQuery;
	var winheight = $(window).height();
	$(target).height(winheight);
}

function posSlideContent(){
	var $ = jQuery;
	
	$('.slide-content').each(function(){
		var scHeight = $(this).height();
		var scWidth = $(this).width();
		$(this).css('margin-top', '-'+(scHeight / 2)+'px');
//		$(this).css('margin-left', '-'+((scWidth / 2) + 40)+'px');
	});
}