 ( function( $ ) {

    function addBlacklistClass() {
        $( 'a' ).each( function() {
            if ( this.href.indexOf('/wp-admin/') !== -1 ||
                this.href.indexOf('/wp-login.php') !== -1 ) {
                $( this ).addClass( 'wp-link' );
            }
        });
    }

    $( function() {

        addBlacklistClass();

        var settings = {
            anchors: 'a',
            blacklist: '.wp-link',
            onBefore: function() {
                $('#load').addClass('loading');
                $('#load-container').addClass('loading');
                //$('html, body').animate({ scrollTop: "0px" }, 'slow');
            },
            onStart: {
                duration: 1000,
                render: function ( $container ) {
                    $container.addClass( 'fade-effect' );

                }
            },
            onAfter: function( $container ) {

                addBlacklistClass();

                var $hash = $( window.location.hash );

                if ( $hash.length !== 0 ) {

                    var offsetTop = $hash.offset().top;

                    $( 'body, html' ).animate( {
                        scrollTop: ( offsetTop - 60 ),
                    }, {
                        duration: 280
                    } );


                }

                $container.removeClass( 'fade-effect' );
                $('#load').removeClass('loading');
                $('#load-container').removeClass('loading');

            }
        };

        $( '#site' ).smoothState( settings );
    } );

})( jQuery );