CREATE TABLE `oink_yoast_primary_term` (  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,  `post_id` bigint(20) DEFAULT NULL,  `term_id` bigint(20) DEFAULT NULL,  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `created_at` datetime DEFAULT NULL,  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  `blog_id` bigint(20) NOT NULL DEFAULT '1',  PRIMARY KEY (`id`),  KEY `post_taxonomy` (`post_id`,`taxonomy`),  KEY `post_term` (`post_id`,`term_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `oink_yoast_primary_term` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `oink_yoast_primary_term` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
