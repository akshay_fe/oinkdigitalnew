CREATE TABLE `oink_wpgmza_categories` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `active` tinyint(1) NOT NULL,  `category_name` varchar(50) NOT NULL,  `category_icon` varchar(700) NOT NULL,  `retina` tinyint(1) DEFAULT '0',  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `oink_wpgmza_categories` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `oink_wpgmza_categories` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
