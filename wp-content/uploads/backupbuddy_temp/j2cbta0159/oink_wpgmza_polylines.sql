CREATE TABLE `oink_wpgmza_polylines` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `map_id` int(11) NOT NULL,  `polydata` longtext NOT NULL,  `linecolor` varchar(7) NOT NULL,  `linethickness` varchar(3) NOT NULL,  `opacity` varchar(3) NOT NULL,  `polyname` varchar(100) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `oink_wpgmza_polylines` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `oink_wpgmza_polylines` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
