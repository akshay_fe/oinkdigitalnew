CREATE TABLE `oink_wpgmza_polygon` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `map_id` int(11) NOT NULL,  `polydata` longtext NOT NULL,  `innerpolydata` longtext NOT NULL,  `linecolor` varchar(7) NOT NULL,  `lineopacity` varchar(7) NOT NULL,  `fillcolor` varchar(7) NOT NULL,  `opacity` varchar(3) NOT NULL,  `title` varchar(250) NOT NULL,  `link` varchar(700) NOT NULL,  `ohfillcolor` varchar(7) NOT NULL,  `ohlinecolor` varchar(7) NOT NULL,  `ohopacity` varchar(3) NOT NULL,  `polyname` varchar(100) NOT NULL,  `description` text NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `oink_wpgmza_polygon` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `oink_wpgmza_polygon` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
