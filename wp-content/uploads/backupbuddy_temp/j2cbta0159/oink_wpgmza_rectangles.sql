CREATE TABLE `oink_wpgmza_rectangles` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `map_id` int(11) NOT NULL,  `name` text,  `cornerA` point DEFAULT NULL,  `cornerB` point DEFAULT NULL,  `color` varchar(16) DEFAULT NULL,  `opacity` float DEFAULT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `oink_wpgmza_rectangles` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `oink_wpgmza_rectangles` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
