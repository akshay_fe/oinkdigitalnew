CREATE TABLE `oink_itsec_lockouts` (  `lockout_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `lockout_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,  `lockout_start` datetime NOT NULL,  `lockout_start_gmt` datetime NOT NULL,  `lockout_expire` datetime NOT NULL,  `lockout_expire_gmt` datetime NOT NULL,  `lockout_host` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `lockout_user` bigint(20) unsigned DEFAULT NULL,  `lockout_username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `lockout_active` int(1) NOT NULL DEFAULT '1',  PRIMARY KEY (`lockout_id`),  KEY `lockout_expire_gmt` (`lockout_expire_gmt`),  KEY `lockout_host` (`lockout_host`),  KEY `lockout_user` (`lockout_user`),  KEY `lockout_username` (`lockout_username`),  KEY `lockout_active` (`lockout_active`)) ENGINE=MyISAM AUTO_INCREMENT=184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `oink_itsec_lockouts` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `oink_itsec_lockouts` VALUES('183', 'brute_force', '2021-04-20 18:52:52', '2021-04-20 18:52:52', '2021-04-20 19:07:52', '2021-04-20 19:07:52', '2a01:4f8:212:41d::2', NULL, NULL, '1');
INSERT INTO `oink_itsec_lockouts` VALUES('182', 'four_oh_four', '2021-04-15 21:30:56', '2021-04-15 21:30:56', '2021-04-15 21:45:56', '2021-04-15 21:45:56', '146.148.48.207', NULL, NULL, '1');
INSERT INTO `oink_itsec_lockouts` VALUES('179', 'four_oh_four', '2021-04-14 07:04:44', '2021-04-14 07:04:44', '2021-04-14 07:19:44', '2021-04-14 07:19:44', '187.189.158.70', NULL, NULL, '1');
INSERT INTO `oink_itsec_lockouts` VALUES('180', 'four_oh_four', '2021-04-14 07:04:50', '2021-04-14 07:04:50', '2021-04-14 07:19:50', '2021-04-14 07:19:50', '187.189.158.70', NULL, NULL, '1');
INSERT INTO `oink_itsec_lockouts` VALUES('181', 'four_oh_four', '2021-04-15 21:30:52', '2021-04-15 21:30:52', '2021-04-15 21:45:52', '2021-04-15 21:45:52', '146.148.48.207', NULL, NULL, '1');
/*!40000 ALTER TABLE `oink_itsec_lockouts` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
